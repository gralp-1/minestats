## Platform support

| Platform       | Support |
|----------------|---------|
| Fabric         | Full    |
| Sponge         | W.I.P   |
| Quilt          | Planned |
| Forge          | Planned |
| Velocity       | Planned |
| Spigot & forks | Planned |
| Bukkit         | Planned |


## Features
 - Per player configuration on the website
 - Server wide customization which can overide per-player customisation in the config
 - Information on the website:
 	- Health
	- Food & Saturation
	- Dimension
	- Skin
	- Armour
	- Coordinates
	- XP
	- Name & UUID
